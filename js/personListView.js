function PersonListView() {
  this._mHtml = $(
    "<div>" +
      "<h1>Alumnos en el sistema</h1>" +
      "<table id='person_table'>" +
      "<tr><th>NOMBRE </th><th>EDAD </th><th>DOMICILIO</th><th></th></tr>" +
      "</table>" +
      "<h2>DATOS DEL ALUMNO: :</h2>" +
      "<input type='text' id='id_name' placeholder='Nombre del alumno: '/>" +
      "<input type='text' id='id_age' placeholder='Edad: '/>" +
      "<input type='text' id='id_city' placeholder='Domiclio: '/>" +
      "<input id='submit' type='submit' value='Agregar'/>" +
      "</div>"
  );
}

PersonListView.prototype = {
  getHtml: function () {
    return this._mHtml;
  },
  addPersonHandler: function (handler) {
    this._mHtml.find("#submit").click(
      function () {
        var name = this._mHtml.find("#id_name").val();
        var age = this._mHtml.find("#id_age").val();
        var city = this._mHtml.find("#id_city").val();
        this._mHtml.find("input[type='text']").val("");
        handler(name, age, city);
      }.bind(this)
    );
  },
  addPerson: function (personView) {
    this._mHtml.find("#person_table").append(personView.getHtml());
  },
};
